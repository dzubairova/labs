package hw1;

public class Lab5 {
    public static int getNumber(int num, int position) {
        int remainder = Math.abs(num % (int) Math.pow(10, position));
        int result = remainder / (int) Math.pow(10, position -1);
        return result;
    }

    public static void main(String[] args) {
        System.out.println(getNumber(12345, 5));
    }
}
