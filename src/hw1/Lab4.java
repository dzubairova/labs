package hw1;

public class Lab4 {
    public static boolean checkNumber(int number) {
        int mask1 = 0b01000100;
        int mask2 = 0b11101101;
        return (number & mask1) == mask1 && (number | mask2) == mask2;
    }

    public static void main(String[] args) {
        System.out.println(checkNumber(205));
    }
}
