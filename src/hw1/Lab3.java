package hw1;

public class Lab3 {
    public static boolean conditionChecker(int a, int b, int c, int d) {
        return ((a % 2 == 0) && (b % 4 == 0)) || ((c % 3 == 0) && (d % 3 != 0));
    }

    public static void main(String[] args) {
        System.out.println(conditionChecker(2, 4, 5, 0));
    }
}
