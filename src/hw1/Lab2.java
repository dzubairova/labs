package hw1;

public class Lab2 {
    public static void main(String[] args) {
        if (args.length > 0) {
            double num = Double.parseDouble(args[0]);
            double result = (num == 0) ? Double.MAX_VALUE : 100/num;
            System.out.println(result);
        }
    }
}
