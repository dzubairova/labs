package hw1;

public class Lab1 {

    public static void notAnd(boolean a, boolean b) {
        boolean result = !(a && b);
        System.out.printf("| %-5b | %-5b | %-5b |%n", a, b, result);
    }

    public static void notOr(boolean a, boolean b) {
        boolean result = !(a || b);
        System.out.printf("| %-5b | %-5b | %-5b |%n", a, b, result);
    }

    public static void main (String[] args) {
        System.out.println("Logic for NAND Operation:");
        notAnd(false, false);
        notAnd(true, false);
        notAnd(false, true);
        notAnd(true, true);
        System.out.println("\nLogic for NOR Operation:");
        notOr(false, false);
        notOr(true, false);
        notOr(false, true);
        notOr(true, true);
    }

}
