package hw9.user;

import hw9.products.Product;

public class User {
    public void eat(Product product) {
        System.out.printf("Это было %s, вкусно!\n", product.getName());
    }
}
