package hw9.jar;

import hw9.products.Product;

public class Jar {
    private Product product;

    public Jar(Product content) {
        this.product = content;
    }

    public Product getProduct() {
        return this.product;
    }
}
