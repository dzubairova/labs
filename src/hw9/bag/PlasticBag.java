package hw9.bag;

import java.util.ArrayList;
import java.util.Arrays;

public class PlasticBag {
    private ArrayList<Object> content;

    public PlasticBag() {
        this.content = new ArrayList<> ();
    }
    public void putInBag(Object...content) {
        this.content.addAll(Arrays.asList(content));
    }
}
