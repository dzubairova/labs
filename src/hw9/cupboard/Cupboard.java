package hw9.cupboard;

import hw9.jar.Jar;

public class Cupboard {
    private Jar[] jarShelf;

    public Cupboard() {
        this.jarShelf = new Jar[10];
    }

    public Cupboard(int size) {
        this.jarShelf = new Jar[size];
    }

    public Jar getJar(int position) {
        if (this.jarShelf[position] != null) {
            Jar neededJar = jarShelf[position];
            this.jarShelf[position] = null;
            return neededJar;
        } else throw new IllegalArgumentException("На этом месте ничего нет");
    }

    public void putJar(Jar jar) {
        for (int i = 0; i < jarShelf.length; i++) {
            if (jarShelf[i] == null) {
                jarShelf[i] = jar;
                return;
            }
        }
        System.out.println("В шкафу нет свободных мест");
    }
}
