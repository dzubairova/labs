package hw9;

import hw9.animal.Cat;
import hw9.animal.Dog;
import hw9.bag.PlasticBag;
import hw9.cupboard.Cupboard;
import hw9.jar.Jar;
import hw9.products.Product;
import hw9.products.forestgifts.*;
import hw9.products.vegetables.*;
import hw9.robot.Terminator;
import hw9.user.User;
import stub.Simulator;
import test.*;

public class Environment {

    public static void main(String[] args) {
        // Задание 1: Импорт
        System.out.println(TestCase.startTestCase());
        System.out.println(TestChain.startTestChain());
        System.out.println(Simulator.startSimulator());

        // Задание 2: Иерархия пакетов 1
        Dog.sayHello();
        Cat.sayHello();
        Terminator.sayHello();

        // Задание 3: Иерархия пакетов 2
        // Создать шкаф с несколькими банками с продуктами
        Cupboard cupboard = new Cupboard(8);
        cupboard.putJar(new Jar(new Berry()));
        cupboard.putJar(new Jar(new Berry()));
        cupboard.putJar(new Jar(new Cucumber()));
        cupboard.putJar(new Jar(new Tomato()));
        cupboard.putJar(new Jar(new Mushroom()));

        // Достать() какую-либо Банку из шкафа
        Jar myNewJar = cupboard.getJar(1);

        // достать() продукт из банки
        Product food = myNewJar.getProduct();

        //Накормить Пользователя
        User user = new User();
        user.eat(food);

        // Создать Пакет с Пакетами
        PlasticBag plasticBag = new PlasticBag();
        plasticBag.putInBag(new PlasticBag(), new PlasticBag(), new PlasticBag());


        Cupboard smallCupboard = new Cupboard(1);
        //Достать банку с пустой полки
        //smallCupboard.getJar(0);

        //Положить банку в шкаф, когда все места заняты
        smallCupboard.putJar(new Jar(new Berry()));
        smallCupboard.putJar(new Jar(new Berry()));
    }
}
