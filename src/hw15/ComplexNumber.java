package hw15;

public class ComplexNumber {
    private double realPart;
    private double imaginaryPart;

    @Override
    public String toString() {
        return realPart + " + " + imaginaryPart + "i";
    }

    public ComplexNumber(double realPart, double imaginaryPart) {
        this.realPart = realPart;
        this.imaginaryPart = imaginaryPart;
    }

    public double getRealPart() {
        return realPart;
    }

    public double getImaginaryPart() {
        return imaginaryPart;
    }

    public ComplexNumber add(ComplexNumber n2) {
        double realPart = this.getRealPart() + n2.getRealPart();
        double imaginaryPart = this.getImaginaryPart() + n2.getImaginaryPart();
        return new ComplexNumber(realPart, imaginaryPart);
    }

    public ComplexNumber multiply(ComplexNumber n2) {
        double realPart = this.getRealPart() * n2.getRealPart() - this.getImaginaryPart() * n2.getImaginaryPart();
        double imaginaryPart = this.getImaginaryPart() * n2.getRealPart() + this.getRealPart() * n2.getImaginaryPart();
        return new ComplexNumber(realPart, imaginaryPart);
    }
}
