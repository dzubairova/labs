package hw15;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ComplexNumberTest {
    private ComplexNumber n1, n2, n3;

    @BeforeEach
    public void initTest() {
        ///GIVEN
        n1 = new ComplexNumber(2, 3);
        n2 = new ComplexNumber(3, -1);
        n3 = new ComplexNumber(4, 2);
    }

    @Test
    @DisplayName("Addition of 2 complex numbers")
    public void addComplexNumbers() {
        ///WHEN
        double actualResultReal = n1.add(n2).getRealPart();
        double actualResultImaginary = n1.add(n2).getImaginaryPart();

        ///THEN
        double expectedResultReal = 5.0;
        double expectedResultImaginary =  + 2.0;
        assertEquals(expectedResultReal, actualResultReal);
        assertEquals(expectedResultImaginary, actualResultImaginary);

    }

    @Test
    @DisplayName("Addition of 2 complex numbers - Commutativity check")
    public void addComplexNumbersCommutative() {
        ///WHEN
        double actualResultReal = n2.add(n1).getRealPart();
        double actualResultImaginary = n2.add(n1).getImaginaryPart();

        ///THEN
        double expectedResultReal = 5.0;
        double expectedResultImaginary =  + 2.0;
        assertEquals(expectedResultReal, actualResultReal);
        assertEquals(expectedResultImaginary, actualResultImaginary);
    }

    @Test
    @DisplayName("Addition of 3 complex numbers - Associativity check")
    public void addComplexNumbersAssociative() {
        ///WHEN
        double actualResultReal = n1.add(n2).add(n3).getRealPart();
        double actualResultImaginary = n1.add(n2).add(n3).getImaginaryPart();

        ///THEN
        double expectedResultReal = n3.add(n2).add(n1).getRealPart();
        double expectedResultImaginary = n3.add(n2).add(n1).getImaginaryPart();
        assertEquals(expectedResultReal, actualResultReal);
        assertEquals(expectedResultImaginary, actualResultImaginary);

    }

    @Test
    @DisplayName("Multiplication of 2 complex numbers")
    public void multiplyComplexNumbers() {
        ///WHEN
        double actualResultReal = n1.multiply(n2).getRealPart();
        double actualResultImaginary = n1.multiply(n2).getImaginaryPart();

        ///THEN
        double expectedResultReal = 9.0;
        double expectedResultImaginary =  + 7.0;
        assertEquals(expectedResultReal, actualResultReal);
        assertEquals(expectedResultImaginary, actualResultImaginary);
    }

    @Test
    @DisplayName("Multiplication of 2 complex numbers - Commutativity check")
    public void multiplyComplexNumbersCommutative() {
        ///WHEN
        double actualResultReal = n2.multiply(n1).getRealPart();
        double actualResultImaginary = n2.multiply(n1).getImaginaryPart();

        ///THEN
        double expectedResultReal = 9.0;
        double expectedResultImaginary =  + 7.0;
        assertEquals(expectedResultReal, actualResultReal);
        assertEquals(expectedResultImaginary, actualResultImaginary);
    }

    @Test
    @DisplayName("Multiplication of 3 complex numbers - Associativity check")
    public void multiplyComplexNumbersAssociative() {
        ///WHEN
        double actualResultReal = n2.multiply(n1).multiply(n3).getRealPart();
        double actualResultImaginary = n2.multiply(n1).multiply(n3).getImaginaryPart();

        ///THEN
        double expectedResultReal = n3.multiply(n2).multiply(n1).getRealPart();
        double expectedResultImaginary = n3.multiply(n2).multiply(n1).getImaginaryPart();
        assertEquals(expectedResultReal, actualResultReal);
        assertEquals(expectedResultImaginary, actualResultImaginary);
    }

    @Test
    @DisplayName("Method \"add\" throws Exception on null")
    public void addNull() {
        assertThrows(NullPointerException.class, () -> n1.add(null));
    }

    @Test
    @DisplayName("Method \"multiply\" throws Exception on null")
    public void multiplyNull() {
        assertThrows(NullPointerException.class, () -> n1.multiply(null));
    }
}