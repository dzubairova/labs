package hw10;

public class Url {
    private String schema;
    private String resource;
    private String hostname;

    public Url(String hostname) {
        this.schema = "http";
        this.resource = "";
        this.hostname = hostname;
    }

    public void setSchema(String schema) {
        this.schema = schema;
    }

    public void setResource(String resource) {
        this.resource = resource;
    }

    public String getSchema() {
        return this.schema;
    }

    public String getResource() {
        return resource;
    }

    public String getHostname() {
        return this.hostname;
    }
}
