package hw10;

public class BasicCat {
    private static int numKittens;
    private static BasicCat[] arrayOfCats = new BasicCat[3];
    public static int catsCount = 0;
    public static final double ARRAY_CAPACITY_RATIO = 0.8;

    private String name;
    private int age;
    private int weight;

    BasicCat(String name, int age, int weight) {
        this.name = name;
        this.age = age;
        this.weight = weight;
        if (this.age < 1) {
            numKittens++;
        }
        arrayOfCats[catsCount] = this;
        catsCount++;
        if (catsCount > arrayOfCats.length * ARRAY_CAPACITY_RATIO) {
            BasicCat[] temp = arrayOfCats;
            arrayOfCats = new BasicCat[temp.length * 2];
            for (int i = 0; i < temp.length; i++) {
                arrayOfCats[i] = temp[i];
            }
        }
    }

    public int getAge() {
        return this.age;
    }

    public static int getNumberOfCats() {
        return catsCount;
    }

    public static void main(String[] args) {
        BasicCat kokos = new BasicCat("Kokos", 3, 5);
        BasicCat asya = new BasicCat("Asya", 9, 5);
        BasicCat eva = new BasicCat("Eva", 2, 5);
        BasicCat sonya = new BasicCat("Sonya", 12, 5);
        System.out.println(kokos.getAge());
        System.out.println(getNumberOfCats());
    }
}
