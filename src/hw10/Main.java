package hw10;

public class Main {
    public static final int HUMAN_YEARS = 7;

    public static int catToHumanAge(BasicCat basicCat) {
        return basicCat.getAge() * HUMAN_YEARS;
    }

    public static void main(String[] args) {
        BasicCat bc = new BasicCat("Miaw", 5, 5);
        System.out.println(catToHumanAge(bc));
    }
}
