package hw10;

public class UrlBuilder {
    private Url url;

    UrlBuilder(String hostname) {
        this.url = new Url(hostname);
    }

    public UrlBuilder setSchema(String schema) {
        this.url.setSchema(schema);
        return this;
    }

    public UrlBuilder setResource(String resource) {
        this.url.setResource(resource);
        return this;
    }

    public String build() {
        return this.url.getSchema() + "://" + this.url.getHostname() + "/" + this.url.getResource();
    }

    public static void main(String[] args) {
        String url1 = new UrlBuilder("google.com").setSchema("https").setResource("users").build(); // url при этом содержит https://google.com/users
        String url2 = new UrlBuilder("yandex.ru").build(); // url при этом содержит http://yandex.ru
        String url3 = new UrlBuilder("t-systems.com").setSchema("https").setResource("contacts").build(); // url при этом содержит https://t-systems.com/contacts

        System.out.println(url1);
        System.out.println(url2);
        System.out.println(url3);
    }
}
