package hw3;

public class lab12 {
    public static void reverse (int [] elements) {
        int elementsLength = elements.length;
        int temp;
        for (int i = 0; i < elementsLength/2; i++) {
            temp = elements[i];
            elements[i] = elements[elementsLength - i - 1];
            elements[elementsLength - i - 1] = temp;
        }
    }

    public static void main(String[] args) {
        int[] elements = {0, 1, 2, 30, 40, 500, 6000, 700001};
        System.out.println("Элементы первоначального массива:");
        for (int elem: elements) {
            System.out.printf("%d ", elem);
        }
        reverse(elements);
        System.out.println("\nЭлементы массива после реверсирования:");
        for (int elem: elements) {
            System.out.printf("%d ", elem);
        }
    }
}