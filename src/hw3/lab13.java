package hw3;

public class lab13 {
    public static int summarize(int[] numbers) {
        int sum = 0;
        for (int i = 1; i < numbers.length; i+=2) {
            if (numbers[i] >= 0) {
                sum += numbers[i];
            }
        }
        return sum;
    }

    public static void main(String[] args) {
        int[] numbers = {-1, 23, 34, -7, 0, -77, -952, -2222222};
        System.out.println(summarize(numbers));
    }
}
