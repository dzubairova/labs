package hw8;

import java.util.ArrayList;
import java.util.Arrays;

public class Company {
    private String name;
    private String address;
    private String sphere;
    private ArrayList<Employee> employees;

    Company(String name, String address, String sphere) {
        this.name = name;
        this.address = address;
        this.sphere = sphere;
        this.employees = new ArrayList<>();
    }

    public void addEmployees(ArrayList<Employee> employees) {
        this.employees.addAll(employees);
    }

    private String getEmployeeByPosition(String position) {
        for (Employee em : this.employees) {
            if (em.getPosition().equals(position)) {
                return em.getName();
            }
        }
        return "No employee with such position";
    }

    protected String getCeoName() {
        return getEmployeeByPosition("CEO");
    }

    public String getOwnerName() {
        return getEmployeeByPosition("Owner");
    }

    public int getAverageSalary() {
        int averageSalary = 0;
        for (Employee em : this.employees) {
            averageSalary += em.getSalary();
        }
        return averageSalary / employees.size();
    }

    public int getMaxSalary() {
        int maxSalary = 0;
        for (Employee em : employees) {
            if (maxSalary < em.getSalary()) {
                maxSalary = em.getSalary();
            }
        }
        return maxSalary;
    }

    public static void main(String[] args) {
        Company jetBrains = new Company("JetBrains", "St-Petersburg, 13-Ya Liniya Vasilyevskogo Ostrova, 14", "IT");
        ArrayList<Employee> jetBrainsEmployees = new ArrayList<>(Arrays.asList(
                new Employee("Alice Brown", "Programmer", 200000),
                new Employee("Bob Brown", "Tester", 200000),
                new Employee("John Doe", "CEO", 600000),
                new Employee("David Walles", "HR", 95000),
                new Employee("Mickey Mouse", "Owner", 200000)));

        jetBrains.addEmployees(jetBrainsEmployees);

        Company tSystems = new Company("T-Systems", "St-Petersburg, Primorskiy Prospekt, 70", "IT");
        ArrayList<Employee> tSystemsEmployees = new ArrayList<>(Arrays.asList(
                new Employee("Roy Betty", "Programmer", 100000),
                new Employee("Rick Deckard", "Tester", 100000),
                new Employee("Sarah Connor", "CEO", 450000),
                new Employee("Mary Smith", "HR", 55000),
                new Employee("Helen Ripley", "Owner", 250000)));
        tSystems.addEmployees(tSystemsEmployees);

        Company dell = new Company("DELL", "St-Petersburg, Sredniy Prospect V.O. 36/40", "IT");
        ArrayList<Employee> dellEmployees = new ArrayList<>(Arrays.asList(
                new Employee("Mike Vazowski", "Programmer", 150000),
                new Employee("Mary Antoinette", "Tester", 100000),
                new Employee("Mark Antony", "CEO", 300000),
                new Employee("Peter Parker", "HR", 70000),
                new Employee("Saul Goodman", "HR", 150000)));
        dell.addEmployees(dellEmployees);

        System.out.println(jetBrains.getAverageSalary());
        System.out.println(jetBrains.getMaxSalary());
        System.out.println(tSystems.getAverageSalary());
        System.out.println(tSystems.getMaxSalary());
        System.out.println(dell.getAverageSalary());
        System.out.println(dell.getMaxSalary());
        System.out.println(tSystems.getCeoName());
        System.out.println(tSystems.getOwnerName());
    }
}
