package hw8;

public class Employee {
    private String name;
    private String position;
    private int salary;

    public Employee(String name, String position, int salary) {
        this.name = name;
        this.position = position;
        this.salary = salary;
    }

    protected int getSalary() {
        return this.salary;
    }

    protected void setSalary(int salary) {
        this.salary = salary;
    }

    protected String getPosition() {
        return this.position;
    }

    protected String getName() {
        return this.name;
    }



}
