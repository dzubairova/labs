package hw6;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;

public class Person {
    private String surname;
    private String name;
    private String patronymic;
    private final LocalDate dateOfBirth;
    private Address address;
    private Person spouse;
    private ArrayList<Person> children;

    public Person(String surname, String name, LocalDate dateOfBirth) {
        this.surname = surname;
        this.name = name;
        this.dateOfBirth = dateOfBirth;
        this.children = new ArrayList<Person>();
    }

    public Person(String surname, String name, LocalDate dateOfBirth, String patronymic) {
        this(surname, name, dateOfBirth);
        this.patronymic = patronymic;

    }

    public Address getAddress() {
        return this.address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public void setSpouse(Person spouse) {
        this.spouse = spouse;
    }

    public void addChildren (Person ... children) {
        Collections.addAll(this.children, children);
    }
}
