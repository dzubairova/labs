package hw6;

import java.time.LocalDate;
import java.time.Month;

public class Solution {
    public static void main(String[] args) {
        Address myAddress = new Address("France", "Paris", "Montmartre", "1",
                12, "12343");
        Address anotherAddress = new Address("Great Britain", "London", "Baker street",
                "221B", 1);

        System.out.println(myAddress);
        System.out.println(anotherAddress);

        Person husband = new Person ("Soprano", "Tony", LocalDate.of(1960, Month.JULY, 29));
        Person wife = new Person ("Soprano", "Carmela", LocalDate.of(1966, Month.DECEMBER, 12));
        Person child1 = new Person ("Soprano", "Meadow", LocalDate.of(1985, Month.APRIL, 1));
        Person child2 = new Person ("Soprano", "Antony Jr.", LocalDate.of(1990, Month.FEBRUARY, 15));

        husband.setAddress(myAddress);
        wife.setAddress(anotherAddress);
        wife.setSpouse(husband);
        husband.setSpouse(wife);
        wife.addChildren(child1, child2);
        husband.addChildren(child2);
    }
}
