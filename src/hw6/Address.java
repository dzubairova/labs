package hw6;

public class Address {
    private String country;
    private String town;
    private String street;
    private String houseNumber;
    private int apartment;
    private String zipCode;

    public Address(String country, String town, String street, String houseNumber, int apartment) {
        this.country = country;
        this.town = town;
        this.street = street;
        this.houseNumber = houseNumber;
        this.apartment = apartment;
    }

    public Address(String country, String town, String street, String houseNumber, int apartment, String zipCode) {
        this(country, town, street, houseNumber, apartment);
        this.zipCode = zipCode;
    }

    @Override
    public String toString() {
        return this.country + ", " + this.town + ", " + this.street + " " + this.houseNumber + " " + this.apartment +
                ", " + this.zipCode;
    }

}
