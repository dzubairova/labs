package hw7;

public class Wrappers {
    public static void main(String[] args){
        //(1) Распарсите сроку doubleNumber в переменную d, используя методы parseDouble и valueOf() класса Double.
        // В чем разница этих методов и какая неявная операция происходит при использовании одного из них?
        String doubleNumber = "2.5";
        double d;
        // Возвращает значение типа Double, потом автораспаковывает его в значение примитвного типа double
        d = Double.valueOf(doubleNumber);

        // Возвращает знаение примитивного типа double
        d = Double.parseDouble(doubleNumber);

        //(2) Вызовите метод incrementInt(...). Какая из двух реализаций будет вызвана и почему имена она?
        int i = 0;
        // Будет вызван метод который принимает на вход примитивное значение типа int, так как тип переменной i - int
        incrementInt(i);

        //(3) Вызовите метод incrementInt(...) для iWrapper. Изменилось ли значение переменной после вызова метода?
        //Попробуйте объяснить результат
        Integer iWrapper = 0;
        // Значение переменной не изменилось, т.к. классы обертки в java не изменяемые
        incrementInt(iWrapper);


        //(4) Пропишите в строчка 27-30 явно преобразования, которые делает компилятор неявно
        // Почему строчка 31 не компилируется?
        d = 2.4;
        i= 1;
        float f = 1.6f;
        long l = 1000;
        byte b = 1;
        short s = 1;
        d = d + (double)s;
        f = f + (float)b;
        l = l + (long)i;
        i = (int)(b + s);
        //b = b + s; Не компилируется, т.к. чтобы переменной b присвоить значение выражения, нужно явно привести к byte
        //b = (byte) (b + s);

        //(5) Напишите простую иерархию классов
        // Создайте по одному объекту классов наследников. Приведите их к базовому классу и обратно, аналогично примеру ниже
        Wrappers wrapper = new Wrappers();
        Wrappers.Cat cat = wrapper.new Cat();
        Wrappers.Bear bear = wrapper.new Bear();
        Wrappers.Animal a = cat;
        cat = (Cat) a;
        a = bear;
        bear = (Bear) a;

    }

    public static void incrementInt(int i){
        i++;
    }

    public static void incrementInt(Integer i){
        i++;
    }

    public class Animal {

    }

    public class Bear extends Animal{

    }

    public class Cat extends Animal{

    }

}