package hw2;

public class Lab8 {
    public static int reverse(int num) {
        int numReverse = 0;
        while (num > 0) {
            numReverse *= 10;
            numReverse += num % 10;
            num /= 10;
        }
        return numReverse;
    }

    public static void main(String[] args) {
        System.out.println(reverse(7890));
    }
}
