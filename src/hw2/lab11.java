package hw2;

public class lab11 {
    public static void heroName() {
        int adjective = (int) Math.round(Math.random() * 6);
        switch (adjective){
            case 0:
                System.out.println("Сырный");
                break;
            case 1:
                System.out.println("Восхитительный");
                break;
            case 2:
                System.out.println("Пушистый");
                break;
            case 3:
                System.out.println("Горячий");
                break;
            case 4:
                System.out.println("Великий");
                break;
            case 5:
                System.out.println("Свободный");
                break;
            case 6:
                System.out.println("Латиноамериканский");
                break;
        }
        int noun = (int) Math.round(Math.random() * 6);
        switch(noun){
            case 0:
                System.out.println("Печенег");
                break;
            case 1:
                System.out.println("Половец");
                break;
            case 2:
                System.out.println("Ведьмак");
                break;
            case 3:
                System.out.println("Таракан");
                break;
            case 4:
                System.out.println("Тиранозавр");
                break;
            case 5:
                System.out.println("Эльф");
                break;
            case 6:
                System.out.println("Лепрекон");
                break;
        }
    }

    public static void main(String[] args) {
        heroName();
    }
}
