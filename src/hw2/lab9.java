package hw2;

public class lab9 {
    public static int average() {
        int sum = 0;
        for (int i = 0; i < 10; i++) {
            sum += Math.pow(3, i);
        }
        return sum / 10;
    }

    public static void main(String[] args) {
        System.out.println(average());
    }
}
