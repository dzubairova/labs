package hw2;

public class lab10 {
    public static void binaryCheck(int num) {
        int remainder;
        int result = 0;
        while (num > 1) {
            remainder = num % 2;
            if (remainder == 0) {
                num /=2;
                result++;
            } else {
                System.out.println("Нет");
                return;
            }
        }
        System.out.printf("Да%nСтепень числа по основанию 2: %d", result);
    }
    public static void main(String[] args) {
        binaryCheck(0);
    }
}
