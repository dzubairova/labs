package hw2;

public class Lab6 {
    public static boolean compareDigits(int a, int b, int c) {
        return (a < b) && (b < c);
    }

    public static void main(String[] args) {
        System.out.println(compareDigits(-2, 4, 8));
    }
}
