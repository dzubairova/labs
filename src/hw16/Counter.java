package hw16;

public class Counter {
    private volatile int counter;
    private CounterState state;

    public Counter() {
        this.counter = 0;
        this.state = CounterState.ZERO;
    }

    public synchronized int get() {
        return counter;
    }

    public int getAndIncrement() {
        if (counter == Integer.MAX_VALUE) {
            setState(CounterState.OVERFLOW);
        } else {
            return UpdateCounter(1);
        }
        return Integer.MAX_VALUE;
    }

    public int getCounter() {
        return counter;
    }

    public int getAndDecrement() {
        if (counter == Integer.MIN_VALUE) {
            setState(CounterState.UNDERFLOW);
        } else {
            return UpdateCounter(-1);
        }
        return Integer.MIN_VALUE;
    }

    private synchronized void setCurrentState() {
        if (counter < 0) {
            setState(CounterState.NEGATIVE);
        } else if(counter > 0) {
            setState(CounterState.POSITIVE);
        } else {
            setState(CounterState.ZERO);
        }
    }

    private void setState(CounterState state) {
        this.state = state;
    }

    private synchronized int UpdateCounter(int inc) {
        this.counter = counter + inc;
        setCurrentState();
        return counter;
    }

    public synchronized CounterState getState() {
        return this.state;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }
}
