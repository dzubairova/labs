package hw16;

public enum CounterState{
    NEGATIVE,
    ZERO,
    POSITIVE,
    OVERFLOW,
    UNDERFLOW
}
