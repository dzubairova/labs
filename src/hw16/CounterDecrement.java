package hw16;

public class CounterDecrement implements Runnable {
    private Counter counter;
    private int interval;
    private int repeat;

    public CounterDecrement(Counter counter) {
        this(counter, 1000, 10);
    }

    public CounterDecrement(Counter counter, int interval, int repeat) {
        this.counter = counter;
        this.interval = interval;
        this.repeat = repeat;
    }

    @Override
    public void run() {
        int i = 0;
        while (i < repeat) {
            System.out.println(counter.getAndDecrement());
            i++;
            try {
                Thread.sleep(interval);
            } catch (InterruptedException e) {
            }
        }
    }
}
