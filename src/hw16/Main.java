package hw16;

public class Main{
    public static void main(String[] args) {
        Counter counter = new Counter();

        CounterIncrement counterIncrement = new CounterIncrement(counter);
        CounterDecrement counterDecrement = new CounterDecrement(counter);

        Thread incThread = new Thread(counterIncrement);
        Thread decThread = new Thread(counterDecrement);

        incThread.start();
        decThread.start();
    }
}
