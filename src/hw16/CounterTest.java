package hw16;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class CounterTest {
    private Counter counter;

    @BeforeEach
    public void initTest() {
        counter = new Counter();
    }

    @Test
    @DisplayName("Get Zero State")
    public void getZeroState() {
        ///GIVEN
        CounterIncrement counterIncrement = new CounterIncrement(counter, 1000, 10);
        CounterDecrement counterDecrement = new CounterDecrement(counter, 1200, 10);

        Thread incThread = new Thread(counterIncrement);
        Thread decThread = new Thread(counterDecrement);

        incThread.start();
        decThread.start();

        ///WHEN
        try {
            Thread.sleep(12000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        CounterState actualResultState = counter.getState();
        int actualResultCounter = counter.getCounter();

        ///THEN
        CounterState expectedResultState = CounterState.ZERO;
        int expectedResultCounter = 0;

        assertEquals(expectedResultState, actualResultState);
        assertEquals(expectedResultCounter, actualResultCounter);
    }

    @Test
    @DisplayName("Get Positive State")
    public void getPositiveState() {
        ///GIVEN
        CounterIncrement counterIncrement = new CounterIncrement(counter, 1000, 9);
        CounterDecrement counterDecrement = new CounterDecrement(counter, 1200, 5);

        Thread incThread = new Thread(counterIncrement);
        Thread decThread = new Thread(counterDecrement);

        incThread.start();
        decThread.start();

        ///WHEN
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        CounterState actualResultState = counter.getState();
        int actualResultCounter = counter.getCounter();

        ///THEN
        CounterState expectedResultState = CounterState.POSITIVE;
        int expectedResultCounter = 4;

        assertEquals(expectedResultState, actualResultState);
        assertEquals(expectedResultCounter, actualResultCounter);
    }

    @Test
    @DisplayName("Get Negative State")
    public void getNegativeState() {
        ///GIVEN
        CounterIncrement counterIncrement = new CounterIncrement(counter, 3000, 3);
        CounterDecrement counterDecrement = new CounterDecrement(counter, 1000, 9);

        Thread incThread = new Thread(counterIncrement);
        Thread decThread = new Thread(counterDecrement);

        incThread.start();
        decThread.start();

        ///WHEN
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        CounterState actualResultState = counter.getState();
        int actualResultCounter = counter.getCounter();

        ///THEN
        CounterState expectedResultState = CounterState.NEGATIVE;
        int expectedResultCounter = -6;

        assertEquals(expectedResultState, actualResultState);
        assertEquals(expectedResultCounter, actualResultCounter);
    }

    @Test
    @DisplayName("Get Overflow State")
    public void getOverflowState() {
        ///GIVEN
        counter.setCounter(Integer.MAX_VALUE - 10);
        CounterIncrement counterIncrement = new CounterIncrement(counter, 500, 15);
        CounterDecrement counterDecrement = new CounterDecrement(counter, 1500, 4);

        Thread incThread = new Thread(counterIncrement);
        Thread decThread = new Thread(counterDecrement);

        incThread.start();
        decThread.start();

        ///WHEN
        try {
            Thread.sleep(8000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        CounterState actualResultState = counter.getState();
        int actualResultCounter = counter.getCounter();

        ///THEN
        CounterState expectedResultState = CounterState.OVERFLOW;
        int expectedResultCounter = Integer.MAX_VALUE;

        assertEquals(expectedResultState, actualResultState);
        assertEquals(expectedResultCounter, actualResultCounter);
    }

    @Test
    @DisplayName("Get Underflow State")
    public void getUnderflowState() {
        ///GIVEN
        counter.setCounter(Integer.MIN_VALUE + 10);
        CounterIncrement counterIncrement = new CounterIncrement(counter, 1500, 4);
        CounterDecrement counterDecrement = new CounterDecrement(counter, 500, 15);

        Thread incThread = new Thread(counterIncrement);
        Thread decThread = new Thread(counterDecrement);

        incThread.start();
        decThread.start();

        ///WHEN
        try {
            Thread.sleep(8000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        CounterState actualResultState = counter.getState();
        int actualResultCounter = counter.getCounter();

        ///THEN
        CounterState expectedResultState = CounterState.UNDERFLOW;
        int expectedResultCounter = Integer.MIN_VALUE;

        assertEquals(expectedResultState, actualResultState);
        assertEquals(expectedResultCounter, actualResultCounter);
    }
}