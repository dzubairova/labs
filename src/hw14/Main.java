package hw14;

import java.util.*;
import java.util.stream.Collectors;

/*
        1) Создайте класс Employee с полями Name, Surname, Salary и соотвествующими сеттерами, геттерами
        и конструктором инициализирующим все поля, перегрузите метод toString

        2) Создайте класс Company с коллекцией(тип коллекции на ваше усмотрение) сотрудников.
        Предоставте методы для добавления и удаления сотрудников в коллецию,а так же для вывода информации о всех сотрудников в консоль.
        Предоставьте необходимыые конструкторы(как минимум коллекцию надо инициализировать).
        Заполните коллекцию с сотрудниками минимум 5 записями.

        3) Дополните класс Company коллекцией слоганов компании. Слоганы должны быть уникальны.
        Предоставьте методы для добавления и получения слогонов

        4) Дополните класс Company коллекцией автомобилей (можно не создавать отдельный класс, достаточно строки-названия авто).
        Каждому автомобилю соотвествует уникальный гос. номер (А999АА78 и т.д.). Добавьте методы для внесения автомобилей в коллекцию,
        а так же метод для получения автомобиля по его номеру. Добавьте несколько автомобилей
         */
public class Main {
    public static void main(String[] args) {
        Employee e1 = new Employee("Alex", "Star", 20000);
        Employee e2 = new Employee("Alexey", "Star", 2110000);
        Employee e3 = new Employee("Andrei", "Star", 210000);
        Employee e4 = new Employee("Alena", "Star", 50000);
        Employee e5 = new Employee("Ali", "Star", 30000);
        Employee e6 = new Employee("Alan", "Alien", 30000);

        HashSet<Employee> staff = new HashSet<Employee>(Arrays.asList(e1, e2, e3, e4, e5, e6));
        Company company = new Company("t-systems", staff);
        company.addEmployee(e6);
        company.removeEmployee(e1);
        company.printEmployees();
        company.addSlogan("Work hard");
        company.addSlogan("Have a great day");
        company.printSlogans();

        company.addCar("A12343", "BMW");
        company.addCar("A12355", "Folkswagen");
        System.out.println(company.getCarByNumber("A12355"));
        System.out.println(company.getCarByNumber("7"));

        /*
         * 1) Напишите функциональный интерфейс который принимает две переменные типа double
         * и результатом работы которого будет так же тип double
         *
         * 2) Напишите несколько лямдб для вычисления площади геометрических фигур:
         * прямоугольника: сторона*сторона
         * треугольника: 0,5*основание*высота
         * круга: пи*радиус^2
         * выведите в консоль результат работы каждой из лямбд
         *
         * 3) Используйте StreamAPI для класса Company из предыдущей части выведите в консоль всех сотрудников чья зарплата выше средней.
         *
         * 4) Используйте StreamAPI для класса Company из предыдущей части выведите сотрудника с минимальной зарплатой.
         *
         * 5) Используя StreamAPI выведите на консоль всех сотрудников отсортированных в естественном порядке (фамилия, имя)
         * */

        Func<Double> rectangleArea = (val1, val2) -> val1 * val2;
        double rectangle = rectangleArea.apply(10.0, 20.0);
        System.out.println("Площадь прямоугольника: " + rectangle);

        Func<Double> triangleArea = (val1, val2) -> 0.5 * val1 * val2;
        double triangle = triangleArea.apply(10.0, 20.0);
        System.out.println("Площадь треугольника: " + triangle);

        Func<Double> circleArea = (val1, val2) -> Math.PI * val1 * val2;
        double circle = circleArea.apply(10.0, 20.0);
        System.out.println("Площадь круга: " + circle);

        double averageSalary = staff
                .stream()
                .mapToInt(Employee::getSalary)
                .average().orElse(0);

        Set<Employee> employeesAboveAverage = staff.stream()
                .filter(employee -> employee.getSalary() > averageSalary).collect(Collectors.toSet());
        System.out.println("Сотрудники, зарплата которых выше среднего: ");
        employeesAboveAverage.forEach(System.out::println);

        Employee e = staff.stream()
                .min(Comparator.comparing(Employee::getSalary))
                .orElseThrow(NoSuchElementException::new);
        System.out.println("Сотрудник с минимальной зарплатой: " + e);

        List<Employee> sortedEmployees = staff.stream()
                .sorted(Comparator.comparing(Employee::getSurname))
                .sorted(Comparator.comparing(Employee::getName))
                .collect(Collectors.toList());

        System.out.println("Сотрудники, отсортированные в естественном порядке (фамилия, имя)");
        sortedEmployees.forEach(System.out::println);
    }
}