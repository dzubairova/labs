package hw14;

import java.util.HashMap;
import java.util.HashSet;

public class Company {
    private String name;
    private HashSet<Employee> staff = new HashSet<Employee>(10);
    private HashSet<String> slogans = new HashSet<String>(10);
    private HashMap<String, String> cars = new HashMap<String, String>(10);

    public Company(String name, HashSet<Employee> staff) {
        this.name = name;
        this.staff.addAll(staff);
    }

    public void removeEmployee (Employee employee) {
        staff.remove(employee);
    }

    public void addEmployee(Employee employee) {
        staff.add(employee);
    }

    public void printEmployees() {
        System.out.printf("Employees of %s:%n", name);
        staff.forEach(System.out::print);
    }

    public void addSlogan(String slogan) {
        this.slogans.add(slogan);
    }

    public void printSlogans() {
        System.out.printf("Slogans of %s:%n", name);
        slogans.forEach(System.out::println);
    }

    public void addCar(String number, String name) {
        cars.put(number, name);
    }

    public String getCarByNumber(String number) {
        return cars.getOrDefault(number, "There is no car with the specified number");
    }




}
