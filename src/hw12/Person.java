package hw12;

public class Person implements Volumetric{
    private double height;
    private double width;
    private double length;

    public Person(double height, double width, double length) {
        this.height = height;
        this.length = length;
        this.width = width;
    }

    @Override
    public double getVolume() {
        return height * length * width;
    }

    public static void main(String[] args) {
        Packet packet = new Packet(5);
        System.out.println(packet.put(new Item(4.5)));
        System.out.println(packet.put(new Item(5)));
        Bag bag = new Bag(7.5);
        System.out.println(bag.put(new Item(1)));
        Person person = new Person(1, 2, 3);
        System.out.println(bag.put(person));
        System.out.println(bag.put(new Person(3, 2, 4.2)));
    }
}
