package hw12;

public class Item implements Volumetric {
    double volume;

    public Item(double volume) {
        this.volume = volume;
    }
    @Override
    public double getVolume() {
        return this.volume;
    }
}
