package hw12;

import java.util.ArrayList;

public abstract class AbstractBag implements Containable {
    private final double maxVolume;
    private double volume;
    private ArrayList<Volumetric> contents;

    public AbstractBag(double maxVolume){
        this.maxVolume = maxVolume;
        this.volume = maxVolume;
        this.contents = new ArrayList<>();
    }

    @Override
    public boolean put(Volumetric content) {
        if (content.getVolume() < this.volume) {
            this.volume -= content.getVolume();
            this.contents.add(content);
            return true;
        }
        return false;
    }
}
