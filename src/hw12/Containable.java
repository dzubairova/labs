package hw12;

public interface Containable {

    boolean put(Volumetric content);
}
