package hw4;

public class Fibonacci {
    public static int tailFib(int num, int previous, int current) {
        if (num <= 2) {
            return current;
        }

        return tailFib(num - 1, current, previous + current);
    }

    public static int iterativeFib(int num) {
        int previous = 1;
        int current = 1;
        int temp;
        for (int i = 2; i < num; i++) {
            temp = current;
            current += previous;
            previous = temp;
        }
        return current;
    }

    public static int recursiveFib(int num) {
        if (num <= 2) {
            return 1;
        }
        return recursiveFib(num - 1) + recursiveFib(num - 2);
    }

    public static void main(String[] args) {
        int n = 10;

        long startTime = System.nanoTime();
        int resultByIterative = iterativeFib(n);
        long endTime = System.nanoTime();
        System.out.printf("Iterative Result: %d %nDuration in nanoseconds: %d%n", resultByIterative, endTime - startTime);

        startTime = System.nanoTime();
        int resultByRecursive = recursiveFib(n);
        endTime = System.nanoTime();
        System.out.printf("Recursive Result: %d %nDuration in nanoseconds: %d%n", resultByRecursive, endTime - startTime);

        startTime = System.nanoTime();
        int resultByTail = tailFib(n, 1, 1);
        endTime = System.nanoTime();
        System.out.printf("Tail Result: %d %nDuration in nanoseconds: %d%n", resultByTail, endTime - startTime);
    }
}
