package hw4;

public class StringPractice {
    //1) Напишите вежливую функцию, которая возвращает следующую строку:
    //"Hello Alice !"
    public static String politeMethod(String name) {
        return "Hello " + name + "!";
    }

    //2) "Оберните" слово word в "обертку" wrap
    // Примеры вызова функций :
    // makeWrapWord("Hello", "(())") -> ((Hello))
    // makeWrapWord("Crabs", "}}{{") -> }}Crabs{{
    //длина wrap = 4 всегда
    public static String makeWrapWord(String word, String wrap) {
        return wrap.substring(0, 2)+ word + wrap.substring(2, 4);
    }

    //3) Напишите функцию, которая возвращает первую половину строки
    public static String half(String s) {
        return s.substring(0, s.length()/2);
    }

    //4) Верните true, если строка оканчивается на "ood"
    public static boolean endsOod(String s) {
        return s.endsWith("ood");
    }

    //5) Верните количество вхождений заданного символа в заданную строку
    public static int charCounter(String s, char c) {
        int counter = 0;
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == c) {
                counter++;
            }
        }
        return counter;
    }

    //6) Преобразуйте число в строку и верните true, если количество символов в строке нечетное
    public static boolean oddLength(double d) {
        String numberAsString = Double.toString(d);
        return numberAsString.length() % 2 != 0;
    }

    //7) Верните количество вхождений искомой строки в строке для поиска (игнорируя регистр)
    //stringCounter("Java programmers love Java", "java") -> 2
    public static int stringCounter(String s, String searchString) {
        int counter = 0;
        int i = 0;
        int foundIndex;

        s = s.toLowerCase();

        while (i < s.length()) {
            foundIndex = s.indexOf(searchString, i);
            i = foundIndex + searchString.length();
            counter++;
        }

        return counter;
    }

    public static void main(String[] args) {
        System.out.println(politeMethod("Dasha"));

        System.out.println(makeWrapWord("Hello", "(())"));
        System.out.println(makeWrapWord("Crabs", "}}{{"));

        System.out.println(half("Dasha5Dasha"));

        System.out.println(endsOod("moood"));

        System.out.println(charCounter("ccc", 'c'));

        System.out.println(stringCounter("Java programmers love Java", "java"));
    }
}