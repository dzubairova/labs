package hw4;

public class RPSSL {
    final static int rock = 0;
    final static int paper = 1;
    final static int scissors = 2;
    final static int spock = 3;
    final static int lizard = 4;

    final static int draw = 100;
    final static int win = 200;
    final static int lose = 300;

    public static String choiceToString(int choice) {
        switch (choice) {
            case rock: return "Камень";
            case spock: return "Спок";
            case paper: return "Бумага";
            case lizard: return "Ящерица";
            case scissors: return "Ножницы";
            default: return "Неизвестно";
        }
    }

    public static String stateToString(int state) {
        switch (state) {
            case draw: return "Ничья";
            case win: return "Победа";
            case lose: return "Поражение";
            default: return "Неизвестно";
        }
    }

    public static int compare(int self, int other) {
        int result = Math.floorMod(self - other, 5);
        if (result == 1 || result == 3) {
            return win;
        } else if (result == 0) {
            return draw;
        }
        return lose;
    }

    public static int getRandomChoice() {
        return (int) Math.round(Math.random() * 4);
    }

    public static void play(int self) {
        int other = getRandomChoice();
        int result = compare(self, other);
        System.out.printf("Ваш ход: %s%n", choiceToString(self));
        System.out.printf("Ход противника: %s%n", choiceToString(other));
        System.out.println(stateToString(result) + "\n");

    }

    public static void main(String[] args) {
        play(rock);
        play(scissors);
        play(paper);
        play(spock);
        play(lizard);
    }
}
