package hw5.geometry;

public class Square extends Rectangle {

    public Square(double width) {
        this.width = width;
        this.length = width;
    }

    public Square() {
        this.width = 1;
        this.length = 1;
    }
}
