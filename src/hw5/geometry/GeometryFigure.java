package hw5.geometry;

public interface GeometryFigure {
    double getArea();
    double getPerimeter();
}
