package hw5.geometry;

public class Circle implements GeometryFigure{
    private double r;

    public Circle(double r) {
        this.r = r;
    }

    public Circle() {
        this.r = 1;
    }

    @Override
    public double getArea() {
        return Math.PI * Math.pow(r, 2);
    }

    @Override
    public double getPerimeter() {
        return 2 * Math.PI * r;
    }
}
