package hw5.bakery;

public class Baker {
    public void setOvenTemperature(Oven oven, int temperature) {
        oven.setTemperature(temperature);
    }

    public void lowerOvenTemperature(Oven oven, int temperature) {
        oven.setTemperature(oven.getTemperature() - temperature);
    }

    public void higherOvenTemperature(Oven oven, int temperature) {
        oven.setTemperature(oven.getTemperature() + temperature);
    }

    public void turnOn(Oven oven) {
        oven.turnOn();
    }

    public void turnOff(Oven oven) {
        oven.turnOff();
    }

    public boolean getOvenState(Oven oven) {
        return oven.isOn();
    }

    public String getOvenContent(Oven oven) {
        return oven.getContent();
    }

    public void putIntoOven(Oven oven, String dish) {
        oven.setContent(dish);
    }

    public void emptyOven(Oven oven) {
        oven.setContent(null);
    }

    public static void main(String[] args) {
        Baker baker = new Baker();
        ElectricOven oven = new ElectricOven();
        System.out.println(baker.getOvenState(oven));
        baker.turnOn(oven);
        baker.setOvenTemperature(oven, 150);
        baker.lowerOvenTemperature(oven, 10);
        baker.higherOvenTemperature(oven, 300);
        System.out.println(oven.getTemperature());
        baker.putIntoOven(oven, "Bread");
        baker.turnOff(oven);
        System.out.println(baker.getOvenContent(oven));
        baker.emptyOven(oven);
        System.out.println(baker.getOvenContent(oven));


    }
}
