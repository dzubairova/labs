package hw5.bakery;

public abstract class Oven {
    private double maxTemperature;
    private double temperature;
    private boolean isOn;
    private String content;

    public Oven(double maxTemperature) {
        this.maxTemperature = maxTemperature;
        this.temperature = 0;
        this.isOn = false;
    }

    public boolean isOn() {
        return this.isOn;
    }

    public double getTemperature() {
        return this.temperature;
    }

    public String getContent() {
        return this.content;
    }

    public void turnOn() {
        this.isOn = true;
    }

    public void turnOff() {
        this.isOn = false;
        this.temperature = 0;
    }

    public void setTemperature(double temperature) {
        if (!this.isOn) {
            throw new IllegalArgumentException("Вы не включили плиту");
        }

        if (temperature < 0) {
            this.temperature = 0;
        } else this.temperature = Math.min(temperature, this.maxTemperature);
    }

    public void setContent(String content) {
        if (content == null) {
            this.content = null;
        } else if (this.content == null) {
            this.content = content;
        } else {
            throw new IllegalArgumentException("В печи уже что-то есть");
        }
    }
}
