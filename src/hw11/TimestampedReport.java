package hw11;

import java.util.Date;

public class TimestampedReport extends Report {

    public TimestampedReport(String text) {
        super(text);
    }

    @Override
    public String getReport() {
        return getNumber() + ": " + new Date(System.currentTimeMillis()) + " " + getText();
    }
}
