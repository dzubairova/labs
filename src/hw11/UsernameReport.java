package hw11;

public class UsernameReport extends Report {

    public UsernameReport(String text) {
        super(text);
    }

    @Override
    public String getReport() {
        return getNumber() + ": " + System.getProperty("user.name") + " " + getText();
    }
}
