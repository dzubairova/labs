package hw11;

public class Report {
    private final String text;
    private final int number;

    private static int numberOfReports = 1;

    public Report(String text) {
        this.text = text;
        this.number = numberOfReports++;
    }

    public int getNumber() {
        return number;
    }

    public String getText() {
        return text;
    }

    public String getReport() {
        return getNumber() + ": " + getText();
    }
}
