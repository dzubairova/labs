package hw13;

import java.io.*;

import static java.lang.System.lineSeparator;

public class FileCopier {

    static void copyFile(String input, String output) {
        File in = new File(input);
        File out = new File(output);

        try(BufferedReader reader = new BufferedReader(new FileReader(in));
            BufferedWriter writer = new BufferedWriter(new FileWriter(out))) {
            String line;
            while((line = reader.readLine()) != null) {
                writer.write(line + lineSeparator());
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        String input = "src/hw13/dir/original.txt"; //any original file path
        String output = "src/hw13/dir/copy.txt"; // any copy file path

        copyFile(input, output);
    }
}
