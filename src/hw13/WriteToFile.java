package hw13;

import java.io.*;

import static hw13.WriteSignature.addSignature;

public class WriteToFile {

    public static void write(File file, String text) {
        try (PrintWriter writer = new PrintWriter(file)) {
            writer.println(text);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        String output = "./src/hw13/dir/out.txt"; // output file path
        String text = String.format("Это%n" + "Файл%n" + "Записанный%n" + "Программой на JAVA!");
        String name = "Зубаирова Дарья";

        File file = new File(output);
        write(file, text);
        addSignature(file, name);
    }
}
