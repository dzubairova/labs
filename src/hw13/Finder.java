package hw13;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

public class Finder {

    private static String readPath() {
        String path = "";
        try(BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            path =  reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return path;
    }

    public void find(String path) {

        File root = new File(path);
        File[] list = root.listFiles();
        if (list == null) {
            return;
        }

        for (File f : list) {
            if (f.isDirectory()) {
                find(f.getAbsolutePath());
                System.out.println("d: " + f.getAbsolutePath());
            } else {
                System.out.println(f.getAbsolutePath());
            }
        }
    }

    public static void main(String[] args) {

        Finder finder = new Finder();
        String path = readPath();
        finder.find(path);
    }
}