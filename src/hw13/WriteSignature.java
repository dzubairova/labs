package hw13;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;

public class WriteSignature {

    public static void addSignature(File file, String name) {
        try (PrintWriter writer = new PrintWriter(new FileOutputStream(file, true))) {
            writer.println("Написанной студентом " + name);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
